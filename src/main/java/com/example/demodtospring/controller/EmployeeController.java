package com.example.demodtospring.controller;

import com.example.demodtospring.dto.EmployeeDto;
import com.example.demodtospring.dto.EmployeeDtoToEmployee;
import com.example.demodtospring.dto.EmployeeToEmployeeDto;
import com.example.demodtospring.entity.Employee;
import com.example.demodtospring.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {


    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeToEmployeeDto employeeToEmployeeDto;

    @Autowired
    private EmployeeDtoToEmployee employeeDtoToEmployee;


    @GetMapping("/employee")
    public ResponseEntity<List<EmployeeDto>> findAll(){

    List<EmployeeDto> employeeDtoList = new ArrayList<>();

    for(Employee employee : employeeService.findAll()){
        employeeDtoList.add(employeeToEmployeeDto.convert(employee));
    }
        return new ResponseEntity<>(employeeDtoList, HttpStatus.OK);
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<EmployeeDto> getEmploye(@PathVariable Integer id){
        Optional <Employee> employee = employeeService.findById(id);
        if(employee.isEmpty()){
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<EmployeeDto>(employeeToEmployeeDto.convert(employee.get()), HttpStatus.OK);

    }

    @PostMapping("/employee")
    public ResponseEntity<EmployeeDto> addEmployee(@RequestBody EmployeeDto employeeDto){
        if(employeeDto.getId()!= null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        employeeService.save(employeeDtoToEmployee.convert(employeeDto));
        return new ResponseEntity<>(HttpStatus.CREATED);

    }


@PutMapping("/employee/{id}")
    public ResponseEntity<EmployeeDto> updateEmployee(@RequestBody EmployeeDto employeeDto, @PathVariable Integer id){
        employeeDto.setId(id);
        employeeService.save(employeeDtoToEmployee.convert(employeeDto));
       return new ResponseEntity<>(HttpStatus.ACCEPTED);

}


@DeleteMapping("/employee/{id}")
    public ResponseEntity<EmployeeDto> deleteEmployeeById(@PathVariable Integer id){
        try{

            employeeService.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }catch (Exception e){

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
}



}
