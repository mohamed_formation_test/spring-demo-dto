package com.example.demodtospring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDtoSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDtoSpringApplication.class, args);
	}

}
