package com.example.demodtospring.dto;

import com.example.demodtospring.entity.Employee;
import com.example.demodtospring.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeDtoToEmployee {

    @Autowired
    EmployeeService employeeService;

    public Employee convert(EmployeeDto employeeDto){

        Employee employee = employeeDto.getId() != null ? employeeService.findById(employeeDto.getId()).get(): new Employee();

        employee.setFirstName(employeeDto.getFirstName());
        employee.setLastName(employeeDto.getLastName());
        employee.setEmail(employeeDto.getEmail());

        return employee;
    }





}
