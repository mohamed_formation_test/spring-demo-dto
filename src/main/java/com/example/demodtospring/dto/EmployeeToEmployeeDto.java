package com.example.demodtospring.dto;

import com.example.demodtospring.entity.Employee;
import com.example.demodtospring.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeToEmployeeDto {

    public EmployeeDto convert(Employee employee){
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setEmail(employee.getEmail());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());

        return employeeDto;
    }






}
